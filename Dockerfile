FROM node:12
LABEL maintainer="Abel Luck <abel@guardianproject.info>"

ENV LANG=C.UTF-8 \
    DEBIAN_FRONTEND=noninteractive \
    NPM_CONFIG_PREFIX=/home/node/.npm-global

USER root

#============================================
# Google Chrome
#============================================
# can specify versions by CHROME_VERSION;
#  e.g. google-chrome-stable=53.0.2785.101-1
#       google-chrome-beta=53.0.2785.92-1
#       google-chrome-unstable=54.0.2840.14-1
#       latest (equivalent to google-chrome-stable)
#       google-chrome-beta  (pull latest beta)
#============================================
ARG CHROME_VERSION="google-chrome-stable"
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
  && echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list \
  && apt-get update -qqy \
  && apt-get -qqy install ${CHROME_VERSION:-google-chrome-stable}  \
     unzip xmlstarlet python3-cairosvg python3-tinycss python3-cssselect imagemagick icnsutils \
  && rm /etc/apt/sources.list.d/google-chrome.list \
  && apt-get -y autoremove --purge \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

COPY test /

USER node


